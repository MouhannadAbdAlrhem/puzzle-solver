<?php

namespace App\Services;

class Node
{
    public $state;
    public $parent;
    public $g;
    public $h;
    public $f;

    public function __construct($state, $parent, $g, $h)
    {
        $this->state = $state;
        $this->parent = $parent;
        $this->g = $g;
        $this->h = $h;
        $this->f = $g + $h;
    }
}
