<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Node;

class PuzzleController extends Controller
{
    public function index()
    {
        return view('index');
    }

    // public function solve(Request $request)
    // {
    //     $initialState = array_map('intval', $request->input('initial_state'));
    //     $finalState = array_map('intval', $request->input('final_state'));

    //     // Validate input format and state
    //     $validationResult = $this->validateInput($initialState, $finalState);
    //     if ($validationResult !== true) {
    //         return view('solution', ['message' => $validationResult]);
    //     }

    //     // Check solvability
    //     if (!$this->isSolvable($initialState, $finalState)) {
    //         return view('solution', ['message' => 'The given puzzle is unsolvable.']);
    //     }

    //     // Solve the puzzle
    //     $solution = $this->aStarAlgorithm($initialState, $finalState);

    //     if (empty($solution)) {
    //         return view('solution', ['message' => 'No solution found.']);
    //     } else {
    //         return view('solution', ['solution' => $solution]);
    //     }
    // }
    public function solve(Request $request)
    {
        $initialState = array_map('intval', $request->input('initial_state'));
        $finalState = array_map('intval', $request->input('final_state'));

        // Validate input format and state
        $validationResult = $this->validateInput($initialState, $finalState);
        if ($validationResult !== true) {
            return view('solution', ['message' => $validationResult]);
        }

        // Check solvability
        if (!$this->isSolvable($initialState, $finalState)) {
            return view('solution', ['message' => 'The given puzzle is unsolvable.']);
        }

        // Solve the puzzle
        $solution = $this->aStarAlgorithm($initialState, $finalState);

        if (empty($solution)) {
            return view('solution', ['message' => 'No solution found.']);
        } else {
            return view('solution', ['solution' => $solution, 'message' => null]);
        }
    }
    private function validateInput($initialState, $finalState)
    {
        // Check if the input states are arrays with 9 elements each
        if (count($initialState) != 9 || count($finalState) != 9) {
            return 'Invalid input: Each state must contain exactly 9 numbers.';
        }

        // Check if each state contains unique numbers from 0 to 8
        if (!$this->isValidState($initialState) || !$this->isValidState($finalState)) {
            return 'Invalid input: Each state must contain numbers from 0 to 8 without duplicates.';
        }

        return true;
    }

    private function isValidState($state)
    {
        return count($state) == 9 && count(array_unique($state)) == 9 && min($state) == 0 && max($state) == 8;
    }

    private function isSolvable($initial, $goal)
    {
        return $this->getInversions($initial) % 2 == $this->getInversions($goal) % 2;
    }

    private function getInversions($state)
    {
        $inversions = 0;
        for ($i = 0; $i < count($state) - 1; $i++) {
            for ($j = $i + 1; $j < count($state); $j++) {
                if ($state[$i] > $state[$j] && $state[$i] != 0 && $state[$j] != 0) {
                    $inversions++;
                }
            }
        }
        return $inversions;
    }

    private function aStarAlgorithm($initial, $goal)
    {
        $openList = [];
        $closedList = [];
        $startNode = new Node($initial, null, 0, $this->heuristic($initial, $goal));
        $openList[] = $startNode;

        while (!empty($openList)) {
            usort($openList, fn ($a, $b) => $a->f <=> $b->f);
            $currentNode = array_shift($openList);
            $closedList[] = $currentNode;

            if ($currentNode->state == $goal) {
                return $this->reconstructPath($currentNode);
            }

            foreach ($this->getNeighbors($currentNode) as $neighbor) {
                if ($this->inList($neighbor, $closedList)) {
                    continue;
                }

                $neighbor->g = $currentNode->g + 1;
                $neighbor->h = $this->heuristic($neighbor->state, $goal);
                $neighbor->f = $neighbor->g + $neighbor->h;

                if ($this->inList($neighbor, $openList)) {
                    $existingNode = $this->findInList($neighbor, $openList);
                    if ($neighbor->g < $existingNode->g) {
                        $existingNode->g = $neighbor->g;
                        $existingNode->f = $neighbor->f;
                        $existingNode->parent = $currentNode;
                    }
                } else {
                    $neighbor->parent = $currentNode;
                    $openList[] = $neighbor;
                }
            }
        }

        return [];
    }

    private function heuristic($state, $goal)
    {
        $distance = 0;
        for ($i = 0; $i < count($state); $i++) {
            if ($state[$i] != 0) {
                $goalIndex = array_search($state[$i], $goal);
                $currentRow = intdiv($i, 3);
                $currentCol = $i % 3;
                $goalRow = intdiv($goalIndex, 3);
                $goalCol = $goalIndex % 3;
                $distance += abs($currentRow - $goalRow) + abs($currentCol - $goalCol);
            }
        }
        return $distance;
    }

    private function getNeighbors($node)
    {
        $neighbors = [];
        $zeroPos = array_search(0, $node->state);
        $row = intdiv($zeroPos, 3);
        $col = $zeroPos % 3;

        $directions = [
            [-1, 0], // up
            [1, 0], // down
            [0, -1], // left
            [0, 1] // right
        ];

        foreach ($directions as $direction) {
            $newRow = $row + $direction[0];
            $newCol = $col + $direction[1];

            if ($newRow >= 0 && $newRow < 3 && $newCol >= 0 && $newCol < 3) {
                $newPos = $newRow * 3 + $newCol;
                $newState = $node->state;
                $newState[$zeroPos] = $newState[$newPos];
                $newState[$newPos] = 0;
                $neighbors[] = new Node($newState, $node, 0, 0);
            }
        }

        return $neighbors;
    }

    private function reconstructPath($node)
    {
        $path = [];
        while ($node != null) {
            array_unshift($path, $node->state);
            $node = $node->parent;
        }
        return $path;
    }

    private function inList($node, $list)
    {
        foreach ($list as $item) {
            if ($item->state == $node->state) {
                return true;
            }
        }
        return false;
    }

    private function findInList($node, $list)
    {
        foreach ($list as $item) {
            if ($item->state == $node->state) {
                return $item;
            }
        }
        return null;
    }
}
