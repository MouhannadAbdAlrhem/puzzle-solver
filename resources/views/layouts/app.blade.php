<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Eight Puzzle Solver</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <style>
        .puzzle-grid {
            display: grid;
            grid-template-columns: repeat(3, 1fr);
            grid-gap: 10px;
        }

        .puzzle-tile {
            text-align: center;
            font-size: 1.5em;
            border: 2px solid #333;
            background-color: #f8f9fa;
            padding: 10px;
        }

        .puzzle-tile.blank {
            background-color: #e9ecef;
        }

        .puzzle-tile:focus {
            background-color: #fff3cd;
            border-color: #ffecb5;
        }

        .animation-grid {
            display: grid;
            grid-template-columns: repeat(3, 100px);
            grid-template-rows: repeat(3, 100px);
            gap: 5px;
        }

        .animation-tile {
            display: flex;
            align-items: center;
            justify-content: center;
            font-size: 2em;
            border: 1px solid #333;
            background-color: #f8f9fa;
            transition: background-color 0.3s, transform 0.3s;
        }

        .animation-tile.blank {
            background-color: #e9ecef;
        }

        .btn:disabled {
            background-color: #ccc;
            border-color: #ccc;
            cursor: not-allowed;
        }
    </style>
</head>

<body>
    <div class="container mt-5">
        @yield('content')
    </div>
</body>

</html>
