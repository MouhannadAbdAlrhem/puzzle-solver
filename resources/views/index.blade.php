@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <h1 class="text-center">Eight Puzzle Solver</h1>
            <p class="text-center text-muted">Note: Use 0 to represent the empty cell</p>
            <form method="POST" action="/solve">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <label for="initial_state">Initial State:</label>
                        <div class="puzzle-grid">
                            @for ($i = 0; $i < 9; $i++)
                                <input type="text" name="initial_state[]" class="puzzle-tile form-control" maxlength="1"
                                    required>
                            @endfor
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label for="final_state">Final State:</label>
                        <div class="puzzle-grid">
                            @for ($i = 0; $i < 9; $i++)
                                <input type="text" name="final_state[]" class="puzzle-tile form-control" maxlength="1"
                                    required>
                            @endfor
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block mt-3">Solve</button>
            </form>
            <div style="margin-left: 35%;">

                <h1>تقديم الطلاب</h1>
                <h2>muhanad_190535</h2>
                <h2>mahmoud_214862</h2>
                <h2>moayad_275518</h2>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('.puzzle-tile').on('input', function() {
                var value = $(this).val();
                if (value.length > 1) {
                    $(this).val(value[0]);
                }
            });
        });
    </script>
@endsection
