@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <h1 class="text-center">Puzzle Solution</h1>
            @if (isset($message) && $message)
                <div class="alert alert-danger text-center">{{ $message }}</div>
            @else
                @if (isset($solution))
                    <div class="puzzle-grid" id="puzzleGrid">
                        @foreach ($solution[0] as $tile)
                            <div class="puzzle-tile{{ $tile == 0 ? ' blank' : '' }}">{{ $tile == 0 ? '' : $tile }}</div>
                        @endforeach
                    </div>
                    <div class="text-center">
                        <button id="startAnimation" class="btn btn-success mt-3">Animate Solution</button>
                    </div>
                @else
                    <div class="alert alert-warning text-center">No solution provided.</div>
                @endif
            @endif
            <div class="text-center mt-3">
                <a href="/" class="btn btn-secondary">Try Another Puzzle</a>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            var solutionSteps = @json($solution ?? []);
            var currentStep = 0;

            function displayStep(step) {
                $('#puzzleGrid').empty();
                for (var i = 0; i < step.length; i++) {
                    $('#puzzleGrid').append('<div class="puzzle-tile' + (step[i] == 0 ? ' blank' : '') + '">' + (
                        step[i] == 0 ? '' : step[i]) + '</div>');
                }
            }

            $('#startAnimation').on('click', function() {
                var $button = $(this);
                $button.prop('disabled', true).text('Animating...');

                if (solutionSteps.length > 0) {
                    currentStep = 0;
                    displayStep(solutionSteps[currentStep]);
                    var interval = setInterval(function() {
                        currentStep++;
                        if (currentStep < solutionSteps.length) {
                            displayStep(solutionSteps[currentStep]);
                        } else {
                            clearInterval(interval);
                            $button.prop('disabled', false).text('Animate Solution');
                        }
                    }, 1000);
                }
            });
        });
    </script>
@endsection
