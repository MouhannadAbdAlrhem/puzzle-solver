<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\PuzzleController;

Route::get('/', [PuzzleController::class, 'index']);
Route::post('/solve', [PuzzleController::class, 'solve']);


Route::post('/image', function (Request $request) {
    dd($request->file('image')->storeAs('images', 'code.png', 'public'));
    Artisan::call('');
})->name('image');
